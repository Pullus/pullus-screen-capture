Pullus Screen Capture 18.04.2023
================================

`pusca` (**P**ull**u**s **S**creen **Ca**pture) is a command line tool to take single snapshots or recordings of MacOS desktop **windows**. The images are stored as gifs (snapshots) or animated gifs (recordings).

### Background

This application was a case study of XPC services, animated gifs and window functions. **The performance for productive screen recording is insufficient.**

Build
-----

Open the project with Xcode and build the target "PullusScreenCapture". 

Run
---

The binary can be started from the terminal.

 - Drag the "PullusScreenCapture.app" from "Products" (Xcode left column) into the terminal.
 - You will see something like:

		/Users/…/Library/Developer/Xcode/DerivedData/PullusScreenCapture-…/Build/Products/Debug/PullusScreenCapture.app

 - Append the path of the binary `/Contents/MacOS/PullusScreenCapture`.
 - and press `Return`

Alternatively define a sybolic link `pusca` to the same path:

	ln -s /Users/…/PullusScreenCapture.app/Contents/MacOS/PullusScreenCapture pusca


### Example: Devices

Show all windows:

```
pusca list
```

### Help

```
NAME
    pusca - Pullus Screen Capture

SYNOPSIS
    Captures the content of a desktop window.

    help
        Shows this text.

    list
        Shows a list of possible windows.

    snapshot <window ID>
        Takes a single snapshot of a single window.

    start <window ID>
        Starts the recoding of a single window.

    stop
        Stops and saves the recording.

    cancel
        Stops and discards the recording.

EXAMPLES
    pusca snapshot 429

    Takes a snapshot of the window with the ID 429.

AUTHOR
    Frank Schuster

```


Requirements
------------

Developed and tested with MacOS 10.12 and Xcode 9.2.

This software was intentionally developed for macOS 10.12 to be able to continue using older devices.
