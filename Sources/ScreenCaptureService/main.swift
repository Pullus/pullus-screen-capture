import Foundation

///
/// This class initialize
/// - the ServiceDelegate to handle incomming connections and to create a
///   Service for every connection
///
/// # Communication
///
/// ````
///     NSXPCListener ---uses--+
///                            |
///                            V
///                      ServiceDelegate
///                            |
///                creates service instance and
///                  binds it to a connection
///                            |
///                            V
///         Connection --> ScreenCaptureService --+
///                                               |
///         Connection --> ScreenCaptureService --+---> SharedContext --> WindowRecorder
///                                               |
///         Connection --> ScreenCaptureService --+
/// ````
///
class Main {
	
	// MARK: - XPC service
	
	///
	/// The listener for this service. It will handle all incoming connections.
	///
	let serviceListener: NSXPCListener
	
	///
	/// The delegate for the service listener.
	///
	/// HINT: Must be a strong reference
	///
	// #swiftlint:disable weak_delegate
	var serviceListenerDelegate: ServiceListenerDelegate?
	// #swiftlint:enable weak_delegate

	// MARK: -
	
	init() {
		serviceListener = NSXPCListener.service()
		serviceListenerDelegate = ServiceListenerDelegate()
		
		serviceListener.delegate = serviceListenerDelegate
	}
	
	// MARK: -
	
	func run() {
		// Resuming the `serviceListener` starts this service. This method does
		// not return.
		serviceListener.resume()
	}
	
}

Main().run()
