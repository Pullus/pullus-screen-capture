import CoreGraphics
import AppKit

extension CGImage {
	
	///
	/// The length of the longest edge.
	///
	var maxEdgeLength: Int { return max(width, height) }
	
}
