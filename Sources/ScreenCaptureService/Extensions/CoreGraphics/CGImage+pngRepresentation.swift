import AppKit

extension CGImage {
	
	///
	/// Creates the png representation of this image.
	///
	func pngRepresentation(compressedBy compressionFactor: Float) -> Data? {
		// HINT: The `.compressionFactor` "…Identifies an NSNumber object
		//       containing the compression factor of the image…".
		//       `Float` was chosen to avoid `NSNumber`.
		return NSBitmapImageRep(cgImage: self).representation(
			using: .png,
			properties: [.compressionFactor : compressionFactor])
	}
	
}
