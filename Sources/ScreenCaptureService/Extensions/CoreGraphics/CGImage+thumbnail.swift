import CoreGraphics
import AppKit

extension CGImage {

	///
	/// Creates a thumbnail of this image.
	///
	func thumbnail(scaledBy aFactor: Float, compressedBy: Float) -> CGImage? {
		guard
			let pngData = pngRepresentation(compressedBy: compressedBy),
			let imageSource = CGImageSourceCreateWithData(pngData as CFData, nil)
			else { return nil }
		
		let options = Dictionary(
			cgImageSourceThumbnailMaxPixelSize: Int(aFactor * Float(maxEdgeLength)),
			cgImageSourceCreateThumbnailFromImageAlways: true,
			cgImageSourceCreateThumbnailWithTransform: true)
		
		return CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options as CFDictionary)
	}
	
}

fileprivate
extension Dictionary where Key == String, Value == Any {
	
	///
	/// Creates an "Image Source Option Dictionary".
	///
	init(cgImageSourceThumbnailMaxPixelSize: Int, cgImageSourceCreateThumbnailFromImageAlways: Bool, cgImageSourceCreateThumbnailWithTransform: Bool) {
		self.init()
		
		self[kCGImageSourceThumbnailMaxPixelSize as String] = cgImageSourceThumbnailMaxPixelSize
		self[kCGImageSourceCreateThumbnailFromImageAlways as String] = cgImageSourceCreateThumbnailFromImageAlways
		self[kCGImageSourceCreateThumbnailWithTransform as String] = cgImageSourceCreateThumbnailWithTransform
	}
	
}
