import CoreGraphics

///
/// An OOP adapter to CoreGraphics windows.
///
public
class CGWindow {
	
	public
	let id: CGWindowID
	
	public
	init(id anID: CGWindowID) {
		id = anID
	}
	
	// MARK: -
	
	///
	/// Checks if a window exists.
	///
	public
	var isValid: Bool {
		guard let windowInfos = CGWindowListCopyWindowInfo(.optionIncludingWindow, id) as? [[String:Any]]
			else { return false }
		
		return !windowInfos.isEmpty
	}
	
	///
	/// Creates an image of this window.
	///
	public
	func snapshot() -> CGImage? {
		return CGWindowListCreateImage(
			CGRect.null,
			.optionIncludingWindow,
			id,
			.boundsIgnoreFraming)
	}
	
}
