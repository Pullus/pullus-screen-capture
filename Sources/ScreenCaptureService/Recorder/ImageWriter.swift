import Foundation
import CoreGraphics

///
/// Collects a sequence of images that can be stored later as an animated image.
///
class ImageWriter {
	
	private
	var images: [CGImage] = []
	
	// MARK: -
	
	func append(_ image: CGImage) {
		images.append(image)
	}
	
	///
	///
	///
	func save(interval: TimeInterval, to url: URL, success: () -> Void, failure: (Failure) -> Void) {
		guard let animatedImage = CGImageDestinationCreateWithURL(url as NSURL, kUTTypeGIF, images.count, nil) else {
			failure(.createImageFailed)
			return
		}
		CGImageDestinationSetProperties(animatedImage, ImageProperty.gif(loopCount: 0))

		let imageProperties = ImageProperty.gif(delayTime: interval)
		images.forEach { image in
			CGImageDestinationAddImage(animatedImage, image, imageProperties)
		}
		
		guard CGImageDestinationFinalize(animatedImage) else {
			failure(.writeImageFailed)
			return
		}
		
		success()
	}
	
}

///
/// A helper to create (nested) image property dictionaries.
///
fileprivate
struct ImageProperty {
	
	public
	static func gif(loopCount: Int? = nil, delayTime: TimeInterval? = nil) -> CFDictionary {
		var gifDictionary = [String:Any]()
		
		if let loopCount = loopCount { gifDictionary[kCGImagePropertyGIFLoopCount as String] = loopCount }
		if let delayTime = delayTime { gifDictionary[kCGImagePropertyGIFDelayTime as String] = delayTime }
		
		return [kCGImagePropertyGIFDictionary as String : gifDictionary] as CFDictionary
	}

}
