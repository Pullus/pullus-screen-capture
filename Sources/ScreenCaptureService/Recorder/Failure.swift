import Foundation

enum Failure : Error {

	// WindowRecorder.takeSnapshot, WindowRecorder.startRecording
	case invalidWindowID

	// WindowRecorder.takeSnapshot
	case noWindowImage
	case noPNGImage

	// WindowRecorder.startRecording
	case alreadyStarted

	// WindowRecorder.stopRecording, WindowRecorder.cancelRecording
	case notStarted
	
	// ImageWrite.save
	case createImageFailed

	// ImageWrite.save, WindowRecorder.takeSnapshot
	case writeImageFailed

}
