import Foundation
import CoreGraphics

import ScreenCaptureShared

///
/// A window recorder to take a single snapshot or a sequence of snapshots.
///
class WindowRecorder {
	
	private
	var recording: WindowRecording?
	
	// MARK: -
	
	///
	/// Returns a list of `WindowInfo`s from windows that can be used for recording/snaptshot.
	///
	public
	static func allWindowsInfos() -> [WindowInfo] {
		let infoDictionaries = CGWindowListCopyWindowInfo(.optionOnScreenOnly, kCGNullWindowID) as? [[String:Any]]
		
		return infoDictionaries?.flatMap { WindowInfo(infoDictionary: $0) } ?? []
	}
	
	// MARK: - Single Snapshot
	
	public
	func takeSnapshot(of aWindow: CGWindow, url: URL, success: () -> Void, failure: (Failure) -> Void) {
		guard let image = aWindow.snapshot() else {
			failure(.noWindowImage)
			return
		}
		guard let data = image.pngRepresentation(compressedBy: 1) else {
			failure(.noPNGImage)
			return
		}
		
		do {
			try data.write(to: url)
		} catch {
			failure(.writeImageFailed)
			return
		}
		
		success()
	}
	
	// MARK: - Sequence of Snapshots
	
	func startRecording(of aWindow: CGWindow, url: URL, success: () -> Void, failure: (Failure) -> Void) {
		guard recording == nil else {
			failure(.alreadyStarted)
			return
		}
		
		guard aWindow.isValid else {
			failure(.invalidWindowID)
			return
		}

		recording = WindowRecording(window: aWindow, url: url, started: true)
		success()
	}
	
	public
	func stopRecording(success: () -> Void, failure: (Failure) -> Void) {
		guard let recording = recording else {
			failure(.notStarted)
			return
		}
		
		recording.stop(success: success,
					   failure: failure)
		self.recording = nil
	}
	
	public
	func cancelRecording(success: () -> Void, failure: (Failure) -> Void) {
		guard let recording = recording else {
			failure(.notStarted)
			return
		}
		
		recording.cancel()
		self.recording = nil
		success()
	}
	
}
