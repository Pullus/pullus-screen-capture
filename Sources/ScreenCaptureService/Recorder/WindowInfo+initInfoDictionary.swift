import Foundation
import ScreenCaptureShared

public
extension WindowInfo {
	
	///
	/// Creates a `WindowInfo` from a window info dictionary.
	///
	/// See: `CGWindowListCopyWindowInfo`
	///
	public convenience
	init?(infoDictionary: [String:Any]) {
		let windowID = infoDictionary.cgWindowNumber
		let windowName = infoDictionary.cgWindowName
		let windowOwnerPID = infoDictionary.cgWindowOwnerID
		let application = windowOwnerPID.map { NSRunningApplication(processIdentifier: $0) } ?? nil
		let applicationName = application?.localizedName
		
		self.init(windowID, windowName, applicationName)
	}
	
}

// MARK: -

fileprivate
extension Dictionary where Key == String, Value == Any {
	
	///
	/// The window number of a `WindowInfo` dictionary.
	///
	/// See `CGWindowListCopyWindowInfo`.
	///
	var cgWindowNumber: CGWindowID? { return value(for: kCGWindowNumber) }
	
	///
	/// The window name of a `WindowInfo` dictionary.
	///
	/// See `CGWindowListCopyWindowInfo`.
	///
	var cgWindowName: String? { return value(for: kCGWindowName) }
	
	///
	/// The PID of the window owner of a `WindoInfo` dictionary.
	///
	/// See `CGWindowListCopyWindowInfo`.
	///
	var cgWindowOwnerID: pid_t? { return value(for: kCGWindowOwnerPID) }
	
	///
	/// Helper to implement property accessors.
	///
	private
	func value<RESULT>(for key: CFString) -> RESULT? {
		return self[key as String] as? RESULT
	}
	
}
