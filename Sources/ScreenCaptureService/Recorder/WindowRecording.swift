import os.log
import Foundation
import CoreGraphics

import ScreenCaptureShared

class WindowRecording {
	
	private
	let window: CGWindow
	
	private
	let url: URL
	
	private
	let framesPerSecond: Double
	
	private
	let scalingFactor: Float = 0.7
	
	private
	let compressionRatio: Float = 1.0
	
	private
	let timerSource: DispatchSourceTimer

	private
	let imageWriter = ImageWriter()
	
	// MARK: -
	
	init(window aWindow: CGWindow, url anURL: URL, framesPerSecond fps: Double = 10.0, started: Bool = false) {
		window = aWindow
		url = anURL
		framesPerSecond = fps
		timerSource = DispatchSource.makeTimerSource(queue: .main)
		
		timerSource.schedule(deadline: DispatchTime.now(),
							 repeating: .milliseconds(Int(1000.0 / framesPerSecond)))
		timerSource.setEventHandler(handler: DispatchWorkItem(block: { self.snapshot() } ))
		
		if started { start() }
	}
	
	deinit {
		timerSource.cancel()
	}
	
	// MARK: -
	
	func start() {
		timerSource.resume()
	}
	
	private
	func snapshot() {
		guard let image = window.snapshot() else {
			os_log("Cannot get image from window %i.", type: .error, window.id)
			return
		}
		// TODO: Use CoreImage?
		guard let resizedImage = image.thumbnail(scaledBy: scalingFactor, compressedBy: compressionRatio) else {
			os_log("Cannot create thumbnail for image of window %i.", type: .error, window.id)
			return
		}

		imageWriter.append(resizedImage)
	}
	
	public
	func stop(success: () -> Void, failure: (Failure) -> Void) {
		timerSource.cancel()
		imageWriter.save(interval: 1.0 / framesPerSecond,
						 to: url,
						 success: success,
						 failure: failure)
	}
	
	public
	func cancel() {
		timerSource.cancel()
	}
	
}
