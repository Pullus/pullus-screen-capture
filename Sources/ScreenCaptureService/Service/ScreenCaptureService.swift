import Foundation

import ScreenCaptureShared

///
/// A thin adapter from XPC to the business logic (recorder).
///
/// A service instance uses a single recorder and all service instances share
/// this recorder.
///
/// It is intended that a service instance uses only one recorder. That all
/// service instances use the same recorder is a limitation. It simplifies the
/// implementation but excludes multiple/parallel recordings.
///
public
class ScreenCaptureService : ScreenCaptureServiceProtocol {
	
	///
	/// The context shared by all `ScreenCaptureService` instances.
	///
	let context: ScreenCaptureContext
	
	// MARK: -
	
	init(using aContext: ScreenCaptureContext) {
		context = aContext
	}
	
	// MARK: - ScreenCaptureServiceProtocol
	
	public
	func windows(reply aReply: (Status, [WindowInfo]) -> Void) {
		context.queue.sync {
			let windowInfos = WindowRecorder.allWindowsInfos()
			
			aReply(.ok, windowInfos)
		}
	}
	
	// TODO: Check queue usage.
	//       The `takeSnapshot` and `…Recording` methods are called from the XPC
	//       framework and its queue. The `success` and `failure` closures that
	//       triggers the XPC reply are called from the `context.queue`.
	//
	//       Is it problematic that the reply is sent by a different queue?
	
	public
	func takeSnapshot(of aWindowID: CGWindowID, to anURL: URL, reply aReply: (Status) -> Void) {
		context.queue.sync {
			context.recorder.takeSnapshot(of: CGWindow(id: aWindowID),
										  url: anURL,
										  success: { aReply(.ok) },
										  failure: { error in aReply(Status(error)) } )
		}
	}
	
	public
	func startRecording(of aWindowID: CGWindowID, url: URL, reply aReply: (Status) -> Void) {
		context.queue.sync {
			context.recorder.startRecording(of: CGWindow(id: aWindowID),
											url: url,
											success: { aReply(.ok) },
											failure: { error in aReply(Status(error)) } )
		}
	}
	
	public
	func stopRecording(reply aReply: (Status) -> Void) {
		context.queue.sync {
			context.recorder.stopRecording(success: { aReply(.ok) },
										   failure: { error in aReply(Status(error)) } )
		}
	}
	
	public
	func cancelRecording(reply aReply: (Status) -> Void) {
		context.queue.sync {
			context.recorder.cancelRecording(success: { aReply(.ok) },
											 failure: { error in aReply(Status(error)) } )
		}
	}
	
}
