import ScreenCaptureShared

extension Status {
	
	init(_ failure: Failure) {
		switch failure {
		case .invalidWindowID:		self = .invalidWindowID
			
		case .noWindowImage:		self = .noImage
		case .noPNGImage:			self = .noImage
			
		case .alreadyStarted:		self = .alreadyStarted
		case .notStarted:			self = .notStarted
			
		case .createImageFailed:	self = .noImage
			
		case .writeImageFailed:		self = .writeError
		}
	}
	
}
