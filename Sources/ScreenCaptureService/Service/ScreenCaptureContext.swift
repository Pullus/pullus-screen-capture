import Foundation

///
/// The `ScreenCaptureContext` is used as a singleton that is shared by all
/// service instances.
///
/// It is used for shared resources and to store state informations. E.g. After
/// one service instance has started a recording, a second service instance can
/// stop the same recording later.
///
class ScreenCaptureContext {
	
	///
	/// A single recorder used by all service instances.
	///
	let recorder = WindowRecorder()

	///
	/// The queue that is used for the recorder
	///
	let queue = DispatchQueue.global(qos: .default)

}
