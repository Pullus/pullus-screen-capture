import Foundation

public
extension DateFormatter {
	
	public convenience
	init(format aFormat: String) {
		self.init()
		
		dateFormat = aFormat
	}
	
}
