import Foundation

let ok = PullusScreenCapture().run(arguments: CommandLine.arguments)

exit(ok ? 0 : 1)
