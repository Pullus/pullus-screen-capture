import CoreGraphics

extension CommandLineTool {
	
	///
	/// Tries to read the first argument from `arguments` and tries to convert
	/// it to a `GCWindowID`.
	///
	/// If the first argument is missing (`arguments` is empty) the specified
	/// exception is raised.
	///
	/// The first argument is removed from the `arguments`.
	///
	func parseCGWindowID(onErrorThrow message: String) throws -> CGWindowID {
		return try parseFirst(String.toCGWindowID, onErrorThrow: message)
	}
	
}
