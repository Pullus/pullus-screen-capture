import Foundation
import CoreGraphics

public
class CommandLineTool {
	
	///
	/// The input arguments used as an input queue.
	///
	private
	var arguments: ArraySlice<String> = []
	
	public
	func popFirst() -> String? {
		return arguments.popFirst()
	}
	
	///
	///
	///
	public
	func acceptOptionalString(_ text: String) -> Bool {
		guard arguments.first == text else { return false}

		arguments.removeFirst()
		
		return true
	}
	
	///
	/// Tries to read the first argument from `arguments` and tries to "convert"
	/// it to a string.
	///
	/// If the first argument is missing (`arguments` is empty) the specified
	/// exception is raised.
	///
	/// The first argument is removed from the `arguments`.
	///
	public
	func parseString(onErrorThrow message: String) throws -> String {
		guard
			let value = popFirst()
			else { throw CommandLineFailure(message) }
		
		return value
	}
	
	///
	/// Tries to read the first argument from `arguments` and tries to convert
	/// it to a float.
	///
	/// If the first argument is missing (`arguments` is empty) the specified
	/// exception is raised.
	///
	/// The first argument is removed from the `arguments`.
	///
	public
	func parseFloat(onErrorThrow message: String) throws -> Float {
		return try parseFirst(String.toFloat, onErrorThrow: message)
	}
	
	///
	/// Tries to read the first argument from `arguments` and tries to convert
	/// it to an integer.
	///
	/// If the first argument is missing (`arguments` is empty) the specified
	/// exception is raised.
	///
	/// The first argument is removed from the `arguments`.
	///
	public
	func parseInt(onErrorThrow message: String) throws -> Int {
		return try parseFirst(String.toInt, onErrorThrow: message)
	}
	
	///
	/// Helper to implement parse methods that convert an argument (`String`)
	/// into a custom type (`RESULT`)
	///
	public
	func parseFirst <RESULT> (_ convert: (String) -> () -> RESULT?, onErrorThrow message: String) throws -> RESULT {
		guard
			let first = popFirst(),
			let result = convert(first)()
			else { throw CommandLineFailure(message) }
		
		return result
	}
	
	// MARK: -
	
	///
	/// Starts the parsing of the arguments and executes the commands.
	///
	public
	func run(arguments anArguments: [String] = CommandLine.arguments) -> Bool {
		do {
			// Drop the name of the executable (puauma)
			arguments = anArguments.dropFirst()
			
			try parse()
			
			guard
				arguments.isEmpty
				else { throw CommandLineFailure("Ignored supernumerary arguments") }
			
			return true
		} catch {
			printUsage(String(describing: error))
			return false
		}
	}
	
	///
	/// Parses the arguments.
	///
	/// It is called by `run` and shoud be overriden by a subclass.
	///
	public
	func parse() throws {
		throw CommandLineFailure("Cannot parse arguments. The `parse()` method is not overriden.")
	}
	
	///
	/// Prints the usage of a command line tool (subclass).
	///
	/// It is called by `run` and other parse methods. It should be overriden by
	/// the subclass.
	///
	/// ````
	/// override
	/// 	func printUsage(_ message: String? = nil) {
	/// 		print("NAME")
	/// 		print("    mycommand - My command")
	/// 		print()
	/// 		print("SYNOPSIS")
	/// 		print("    This command…")
	/// 		print()
	/// 		print("    help")
	/// 		print("        Shows this text.")
	/// 		print()
	/// 		print("    do")
	/// 		print("        Shows ….")
	/// 		print()
	/// 		print()
	/// 		print("EXAMPLES")
	/// 		print("    mycommand do")
	/// 		print()
	/// 		print("    Starts the….")
	/// 		print()
	/// 		print("AUTHOR")
	/// 		print("    Frank Schuster")
	///
	/// 		super.printUsage(message)
	/// 	}
	/// ````
	///
	/// The default implementation prints `message` as an error.
	///
	///  - Parameters:
	///     - message: The error message
	///
	public
	func printUsage(_ message: String? = nil) {
		if let message = message {
			print()
			print("Failure:", message)
		}
	}
	
}
