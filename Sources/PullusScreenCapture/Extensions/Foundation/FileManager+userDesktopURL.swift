import Foundation

public
extension FileManager {

	public
	static func userDirectoryURL() -> URL? {
		return FileManager.default.urls(for: .desktopDirectory, in: .userDomainMask).first
	}
	
}
