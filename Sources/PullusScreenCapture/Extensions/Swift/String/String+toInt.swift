import Swift

public
extension String {
	
	///
	/// Convenience function for chaining.
	///
	public
	func toInt() -> Int? {
		return Int(self)
	}
	
}

public
extension Substring {
	
	public
	func toInt(radix: Int) -> Int? {
		return Int(self, radix: radix)
	}
	
}
