import CoreGraphics

public
extension String {
	
	///
	/// Convenience function for chaining.
	///
	public
	func toCGWindowID() -> CGWindowID? {
		return CGWindowID(self)
	}
	
}
