import Swift

public
extension Array {
	
	///
	///
	///
	public
	func removedFirst() -> Array {
		guard !isEmpty else { return [] }
		
		var tail = self
		
		tail.removeFirst()
		
		return tail
	}
	
}
