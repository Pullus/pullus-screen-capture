import Foundation

struct Defaults {
	
	static let dateFormatter = DateFormatter(format: "dd.MM.yyyy HH.mm.ss")
	
	private
	func targetFileURL(baseName: String, extension: String) -> URL? {
		let fileName: String = baseName + "-" + Defaults.dateFormatter.string(from: Date()) + "." + `extension`
		
		return FileManager.userDirectoryURL()?.appendingPathComponent(fileName, isDirectory: false)
	}
	
	var snapshotURL: URL? { return targetFileURL(baseName: "Snapshot", extension: "gif") }
	
	var recordingURL: URL? { return targetFileURL(baseName: "Snapshots", extension: "gif") }
	
}
