import ScreenCaptureShared

private
let messages: [Status:String] = [
	.ok : "ok",
	.invalidWindowID : "Invalid window",
	.noImage : "Cannot create image",
	.alreadyStarted : "Recorder already started",
	.notStarted : "Recorder not started",
	.writeError : "Write error"
]

extension Status : CustomStringConvertible {
	
	public
	var description: String { return messages[self] ?? "" }
	
}
