import os.log
import Foundation

import ScreenCaptureShared

class PullusScreenCapture : CommandLineTool {
	
	let defaults = Defaults()
	
	// MARK: - XPC Screen Capture Service
	
	///
	/// The connection to the remote XPC service
	///
	private
	let connection: XPCConnection<ScreenCaptureServiceProtocol>
	
	///
	/// The remote XPC service
	///
	private
	func serviceOrThrow() throws -> ScreenCaptureServiceProtocol {
		guard let service: ScreenCaptureServiceProtocol = connection.remoteObjectProxy(mode: .synchronous) else {
			os_log("The remote service is not accessible.", type: .error )
			throw CommandLineFailure("The capture service is not available.")
		}
		
		return service
	}
	
	// MARK: -
	
	override
	init() {
		connection = XPCConnection.init(serviceName: NSXPCConnection.screenCaptureServiceName)
		connection.remoteObjectInterface?.configureWithScreenCaptureServiceProtocol()
	}
	
	// MARK: -

	//swiftlint:disable function_body_length
	override
	func printUsage(_ message: String? = nil) {
		print("NAME")
		print("    pusca - Pullus Screen Capture")
		print()
		print("SYNOPSIS")
		print("        Captures the content of a desktop window.")
		print()
		print("    help")
		print("        Shows this text.")
		print()
		print("    list")
		print("        Shows a list of possible windows.")
		print()
		print("    snapshot <window ID> (to <file name>)?")
		print("        Takes a single snapshot of a single window.")
		print()
		print("    start <window ID> (to <file name>)?")
		print("        Starts the recoding of a single window.")
		print()
		print("    stop")
		print("        Stops and saves the recording.")
		print()
		print("    cancel")
		print("        Stops and discards the recording.")
		print()
		print("EXAMPLES")
		print("    pusca snapshot 429")
		print()
		print("    Takes a snapshot of the window with the ID 429.")
		print()
		print("AUTHOR")
		print("    Frank Schuster")
		
		super.printUsage(message)
	}
	//swiftlint:enable function_body_length
	
	///
	/// ```
	/// <command> ::= "help"
	///             | "list"
	///             | "snapshot" <window ID> ( to <file name> )?
	///             | "start <window ID>" (to <file name> )?
	///             | "stop"
	///             | "cancel"
	///             .
	///
	/// <window ID> ::= An integer number .
	/// <file name> ::= A file name .
	/// ```
	override
	func parse() throws {
		connection.resume()
		defer { connection.invalidate() }
				
		switch try parseString(onErrorThrow: "Command is missing.") {
		case "help":		printUsage()
		case "list":		try parseList()
		case "snapshot":	try parseSnapshot()
		case "start":		try parseStart()
		case "stop":		try parseStop()
		case "cancel":		try parseCancel()
			
		default:			throw CommandLineFailure("Unknown command")
		}
	}
	
	private
	func parseList() throws {
		let service = try serviceOrThrow()

		service.windows { status, windows in
			switch status {
			case .ok:
				windows
					.sorted { lhs, rhs in (lhs.applicationName, lhs.name ?? "") < (rhs.applicationName, rhs.name ?? "") }
					.forEach { print($0) }
			default:
				print(status)
			}
		}
	}
	
	private
	func parseSnapshot() throws {
		let windowID = try parseCGWindowID(onErrorThrow: "Can't read window ID.")
		guard let url = try parseOptionalToFilename() ?? defaults.snapshotURL else {
			os_log("Cannot take snapshot. No valid file name defined.", type: .error )
			throw CommandLineFailure("No valid file name defined.")
		}
		let service = try serviceOrThrow()

		service.takeSnapshot(of: windowID, to: url) { status in
			print(status)
		}
	}
	
	private
	func parseStart() throws {
		let windowID = try parseCGWindowID(onErrorThrow: "Can't read window ID.")
		guard let url = try parseOptionalToFilename() ?? defaults.recordingURL else {
			os_log("Cannot start recording. No valid file name defined.", type: .error )
			throw CommandLineFailure("No valid file name defined.")
		}
		let service = try serviceOrThrow()

		service.startRecording(of: windowID, url: url) { status in
			print(status)
		}
	}
	
	private
	func parseOptionalToFilename() throws -> URL? {
		guard acceptOptionalString("to") else { return nil }
		
		let fileName = try parseString(onErrorThrow: "Can't read file name.")
		
		return URL(fileURLWithPath: fileName)
	}
	
	private
	func parseStop() throws {
		let service = try serviceOrThrow()

		service.stopRecording { status in
			print(status)
		}
	}
	
	private
	func parseCancel() throws {
		let service = try serviceOrThrow()
		
		service.cancelRecording { status in
			print(status)
		}
	}
	
}
