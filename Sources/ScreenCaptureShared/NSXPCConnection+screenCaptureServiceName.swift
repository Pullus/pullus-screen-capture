import Foundation

extension NSXPCConnection {
	
	///
	/// The common prefix of all screen capture applications
	///
	private static
	let familyID = "org.pullus.screencapture"
	
	///
	/// The base ID of the screen capture service.
	///
	private static
	let serviceBaseID = "ScreenCaptureService"

	///
	/// The name of the XPC service.
	///
	public static
	var screenCaptureServiceName: String { return familyID + "." + serviceBaseID }
	
}
