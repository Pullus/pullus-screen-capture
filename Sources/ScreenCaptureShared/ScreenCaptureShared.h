#import <Cocoa/Cocoa.h>

//! Project version number for ScreenCaptureShared.
FOUNDATION_EXPORT double ScreenCaptureSharedVersionNumber;

//! Project version string for ScreenCaptureShared.
FOUNDATION_EXPORT const unsigned char ScreenCaptureSharedVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ScreenCaptureShared/PublicHeader.h>
