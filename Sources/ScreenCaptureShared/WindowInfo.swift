import CoreGraphics
import AppKit

@objc
public
class WindowInfo : NSObject, NSSecureCoding {
	
	// MARK: - CustomStringConvertible
	
	public override
	var description: String {
		return "\(id)\t\(applicationName)\t\(name ?? "")"
	}
	
	// MARK: - NSSecureCoding
	
	public static
	var supportsSecureCoding: Bool = true
	
	///
	/// Keys for encoding and decoding.
	///
	private
	enum Key : String {
		case id
		case name
		case applicationName
	}
	
	public
	func encode(with aCoder: NSCoder) {
		aCoder.encode(id, forKey: Key.id.rawValue)
		aCoder.encode(name, forKey: Key.name.rawValue)
		aCoder.encode(applicationName, forKey: Key.applicationName.rawValue)
	}
	
	public required convenience
	init?(coder aDecoder: NSCoder) {
		self.init(
			aDecoder.decodeObject(forKey: Key.id.rawValue) as? CGWindowID,
			aDecoder.decodeObject(forKey: Key.name.rawValue) as? String,
			aDecoder.decodeObject(forKey: Key.applicationName.rawValue) as? String)
	}
	
	// MARK: -
	
	public
	let id: CGWindowID
	
	public
	let name: String?
	
	public
	let applicationName: String
	
	// MARK: -
	
	init(id anID: CGWindowID, name aName: String?, applicationName anApplicationName: String) {
		id = anID
		name = aName
		applicationName = anApplicationName
	}
	
	public convenience
	init?(_ anID: CGWindowID?, _ aName: String?, _ anApplicationName: String?) {
		guard
			let anID = anID,
			let anApplicationName = anApplicationName
			else { return nil }
		
		self.init(id: anID, name: aName, applicationName: anApplicationName)
		
	}
	
}
