import Foundation
import CoreGraphics

@objc
public
protocol ScreenCaptureServiceProtocol {
		
	///
	/// Retrieves a list of all visible windows that are associated with an
	/// application.
	///
	func windows(reply: (Status, [WindowInfo]) -> Void)

	///
	/// Takes a snapshot of a window and store it to an URL.
	///
	func takeSnapshot(of aWindowID: CGWindowID, to anURL: URL, reply: (Status) -> Void)

	func startRecording(of aWindowID: CGWindowID, url: URL, reply: (Status) -> Void)
	
	func stopRecording(reply: (Status) -> Void)
	
	func cancelRecording(reply: (Status) -> Void)

}

///
/// Helper to configure a `NSXPCInterface` with the service protocol.
///
extension NSXPCInterface {
	
	///
	/// Adds the allowed/expected custom types of the protocol methods to a
	/// `NSXPCInterface`.
	///
	public
	func configureWithScreenCaptureServiceProtocol() {
		// windows
		add(class: WindowInfo.self, for: #selector(ScreenCaptureServiceProtocol.windows(reply:)), at: 1, ofReply: true)

		// takeSnapshot
		add(class: NSURL.self, for: #selector(ScreenCaptureServiceProtocol.takeSnapshot(of:to:reply:)), at: 1, ofReply: false)

		// startRecording
		add(class: NSURL.self, for: #selector(ScreenCaptureServiceProtocol.startRecording(of:url:reply:)), at: 1, ofReply: false)
	}
	
}
