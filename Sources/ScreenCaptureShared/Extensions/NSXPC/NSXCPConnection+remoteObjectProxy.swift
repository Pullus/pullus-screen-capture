extension NSXPCConnection {
	
	///
	/// The connection mode for the methods.
	///
	/// Apperently the mode effects only methods with a callback like
	/// 'func getMessage(reply aReply: (message) -> Void)'.
	/// Methods with no callback are always asynchronous.
	///
	public
	enum Mode {
		///
		/// For asynchronous callbacks.
		///
		case asynchronous
		
		///
		/// For synchronous callbacks.
		///
		case synchronous
	}
	
	///
	/// Creates a new remote object proxy.
	///
	/// Convenience method to call `remoteObjectProxyWithErrorHandler` or
	/// `synchronousRemoteObjectProxyWithErrorHandler`.
	///
	public
	func remoteObjectProxy(mode aMode: Mode = .asynchronous, errorHandler anErrorHandler: ErrorHandler? = nil ) -> Any {
		let errorHandler = anErrorHandler ?? { error in
			NSLog("The NSXPCConnection to '\(self.serviceName ?? "?")' for the remote object interface '\(String(describing: self.remoteObjectInterface))' failed. Error is: '\(error.localizedDescription)'.")
		}
		
		switch aMode {
		case .asynchronous: return remoteObjectProxyWithErrorHandler(errorHandler)
		case .synchronous:  return synchronousRemoteObjectProxyWithErrorHandler(errorHandler)
		}
	}
	
}
