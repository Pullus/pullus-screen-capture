///
/// A generic version of `NSXPCConnection`.
///
/// The generic type `ObjectProtocol` **must** be a swift protocol. It is the type
/// of the `exportedObject` or the `remoteObjectProxy`.
///
///  - Note: It is not possible to restrict 'ObjectProtocol' to swift protocols.
///  - See: https://stackoverflow.com/questions/47735977/swift-protocol-as-generic-parameter-to-class
///  - See: https://medium.com/ios-os-x-development/types-and-meta-types-in-swift-9cd59ba92295
///
public
class XPCConnection<ObjectProtocol> : NSXPCConnection {
	
	///
	/// Used to initialize the interfaces of the connection.
	///
	public
	enum InterfaceUsage {
		///
		/// Used to configure no interface.
		///
		case none
		
		///
		/// Used to configure the `remoteObjectInterface` for a XPC service
		/// client.
		///
		case remoteInterface
		
		///
		/// Used to configure the `exportedInterface` for a XPC service server.
		///
		case exportedInterface
	}
	
	// MARK: -
	
	// HINT: Needed by super.init/NSXPCConnection.init
	public override
	init() {
		super.init()
	}
	
	#if os(macOS)
	///
	/// Creates a `NSXPCConnection`.
	///
	public
	init(serviceName aServiceName: String, with aUsage: InterfaceUsage = .remoteInterface) {
		super.init(serviceName: aServiceName)
		
		// HINT: The type cast cannot be avoided with the generic constraint
		//       'Connection<ObjectProtocol : AnyObject>'.
		let objectProtocolTypeAsAnyObject = ObjectProtocol.self as AnyObject
		
		// HINT: A direct cast of 'ObjectProtocol.self' to 'Protocol' doesn't
		//       work.
		guard let objectProtocolType = objectProtocolTypeAsAnyObject as? Protocol else {
			NSLog("Can not create the object interface to '\(serviceName ?? "?")'. The type '\(ObjectProtocol.self)' isn't a protocol.")
			return
		}
		
		switch aUsage {
		case .none: break
		case .remoteInterface: remoteObjectInterface = NSXPCInterface(with: objectProtocolType)
		case .exportedInterface: exportedInterface = NSXPCInterface(with: objectProtocolType)
		}
	}
	#endif
	
	///
	/// Creates a new remote object proxy.
	///
	/// Apperently the mode effects only methods with a callback like
	/// 'func getMessage(withReply reply: (message) -> Void)'.
	/// Methods with no callback are always as asynchronous.
	///
	/// HINT: Is it a problem to override (contra variant) a method that was
	///       defined in an extension?
	///       See: http://stackoverflow.com/questions/38213286/ddg#38274660
	///
	public
	func remoteObjectProxy(mode aMode: Mode = .asynchronous, errorHandler anErrorHandler: ErrorHandler? = nil ) -> ObjectProtocol? {
		let errorHandler = anErrorHandler ?? { error in
			NSLog("The connection to '\(self.serviceName ?? "?")' with the protocol '\(ObjectProtocol.self)' failed. Error is: '\(error.localizedDescription)'.")
		}
		
		guard let proxy = super.remoteObjectProxy(mode: aMode, errorHandler: errorHandler) as? ObjectProtocol else {
			NSLog("Cannot create the remote object proxy with the protocol '\(ObjectProtocol.self)' to '\(serviceName ?? "?")'.")
			return nil
		}
		
		return proxy
	}
	
}
