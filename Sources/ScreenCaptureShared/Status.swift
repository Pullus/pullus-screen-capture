import Foundation

@objc
public
enum Status : Int {
	
	case ok
	
	case invalidWindowID
	case noImage
	case alreadyStarted
	case notStarted
	case writeError
	
}
